﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void bDiv_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = mA / mB;
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
            
        }

        private void bMulti_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = mA * mB;
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bMinus_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = mA - mB;
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bPlus_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = mA + mB;
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            lbResult.Text = "0.0";
        }

        private void bSqrt_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Sqrt(mA);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bSin_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Sin(mA);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bCos_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Cos(mB);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bLog_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Log(mA);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bPow_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Pow(mA, mB);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }
    }
}
